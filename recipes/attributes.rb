require 'digest/sha1'

vault_name = node['bigbluebutton']['vault_name']
vault_item = node['bigbluebutton']['vault_item']
secrets = chef_vault_item(vault_name, vault_item)

shared_secret = secrets[node['fqdn']]['api']
hostname = node['bigbluebutton']['hostname'] || node['fqdn']
api_url = "https://#{hostname}/bigbluebutton/api"
server_base_url = "https://#{hostname}"
etherpad_secret = secrets[node['fqdn']]['etherpad']
esl_password = Digest::SHA1.new.update("#{shared_secret}#{etherpad_secret}").hexdigest

default_interface = node['network']['default_interface']
host_ip = node['network']['interfaces'][default_interface]['addresses'].find do |_addr, params|
  params['family'] == 'inet'
end.first

# webserver setup
# this can be an URL where requests to / of the server are redirected.
# This could be your greenlight instance for example
node.default['bigbluebutton']['nginx']['redirect_root_url'] = nil
# should chef require a certificate from letsencrypt?
node.default['bigbluebutton']['nginx']['use_letsencrypt'] = false

# configure settings for bbb-apps-akka here. The options map to a direct setting in the config file
node.default['bigbluebutton']['apps_akka']['services']['bbbWebAPI'] = api_url
node.default['bigbluebutton']['apps_akka']['services']['sharedSecret'] = shared_secret

# configure settings for bbb-fsesl-akka here. The options map to a direct setting in the config file
node.default['bigbluebutton']['fsesl_akka']['freeswitch']['esl']['password'] = esl_password

# configure settings for bbb-web.
node.default['bigbluebutton']['bbb_web']['bigbluebutton.web.serverURL'] = server_base_url
node.default['bigbluebutton']['bbb_web']['securitySalt'] = shared_secret
node.default['bigbluebutton']['bbb_web']['defaultNumDigitsForTelVoice'] = 5

# bbb-html5
node.default['bigbluebutton']['bbb_html5']['public']['kurento']['wsUrl'] = "wss://#{hostname}/bbb-webrtc-sfu"
node.default['bigbluebutton']['bbb_html5']['public']['media']['sipjsHackViaWs'] = true
# BBB <= 2.4
node.default['bigbluebutton']['bbb_html5']['public']['note']['url'] = "#{server_base_url}/pad"
# BBB >= 2.5
node.default['bigbluebutton']['bbb_html5']['public']['pads']['url'] = "#{server_base_url}/pad"
node.default['bigbluebutton']['bbb_html5']['private']['etherpad']['apikey'] = etherpad_secret
node.default['bigbluebutton']['bbb_html5_services']['frontends'] = 2
node.default['bigbluebutton']['bbb_html5_services']['backends'] = 2

# freeswitch
node.default['bigbluebutton']['freeswitch']['default_password'] = esl_password
node.default['bigbluebutton']['freeswitch']['dialin_match'] = false
node.default['bigbluebutton']['freeswitch']['voice_bridge_digits'] = node['bigbluebutton']['bbb_web']['defaultNumDigitsForTelVoice']
node.default['bigbluebutton']['freeswitch']['voice_bridge_max_digits'] = node['bigbluebutton']['freeswitch']['voice_bridge_digits'] + 2
node.default['bigbluebutton']['freeswitch']['ivr_enter_pin'] = 'ivr/ivr-please_enter_pin_followed_by_pound.wav'
node.default['bigbluebutton']['freeswitch']['ivr_invalid_pin'] = 'ivr/ivr-that_was_an_invalid_entry.wav'
node.default['bigbluebutton']['freeswitch']['esl_password'] = esl_password
node.default['bigbluebutton']['freeswitch']['host_ip'] = host_ip
# this is the list of modules which should be enabled in freeswitch.
node.default['bigbluebutton']['freeswitch']['modules'] = {
  'mod_console' => 1,
  'mod_logfile' => 2,
  'mod_event_socket' => 3,
  'mod_sofia' => 4,
  'mod_commands' => 5,
  'mod_conference' => 6,
  'mod_dialplan_xml' => 7,
  'mod_opus' => 8,
  'mod_opusfile' => 9,
  'mod_sndfile' => 10,
  'mod_native_file' => 11,
  'mod_dptools' => 12,
}

# bbb-webrtc-sfu
kurento_params = []
%w(main audio content).each_with_index do |media_type, idx|
  kurento_params[idx] = {
    'ip' => host_ip,
    'url' => "ws://127.0.0.1:#{idx + 8888}/kurento",
    'mediaType' => media_type,
    'ipClassMappings' => {
      'local' => '',
      'private' => '',
      'public' => '',
    },
    'options' => {
      'failAfter' => 5,
      'request_timeout' => 30000,
      'response_timeout' => 30000,
    },
  }
end
node.default['bigbluebutton']['bbb_webrtc_sfu']['kurento'] = kurento_params
node.default['bigbluebutton']['bbb_webrtc_sfu']['balancing-strategy'] = 'MEDIA_TYPE'
node.default['bigbluebutton']['bbb_webrtc_sfu']['freeswitch']['esl_password'] = esl_password
node.default['bigbluebutton']['bbb_webrtc_sfu']['freeswitch']['sip_ip'] = host_ip

# recording
node.default['bigbluebutton']['recording']['bbb_version'] = '2.1.0'
node.default['bigbluebutton']['recording']['raw_audio_src'] = '/var/freeswitch/meetings'
node.default['bigbluebutton']['recording']['kurento_video_src'] = '/var/kurento/recordings'
node.default['bigbluebutton']['recording']['kurento_screenshare_src'] = '/var/kurento/screenshare'
node.default['bigbluebutton']['recording']['mediasoup_video_src'] = '/var/mediasoup/recordings'
node.default['bigbluebutton']['recording']['mediasoup_screenshare_src'] = '/var/mediasoup/screenshare'
node.default['bigbluebutton']['recording']['raw_presentation_src'] = '/var/bigbluebutton'
node.default['bigbluebutton']['recording']['notes_apikey'] = etherpad_secret
node.default['bigbluebutton']['recording']['notes_endpoint'] = 'http://127.0.0.1:9001/p'
node.default['bigbluebutton']['recording']['notes_formats'] = %w(etherpad html pdf)
node.default['bigbluebutton']['recording']['redis_host'] = '127.0.0.1'
node.default['bigbluebutton']['recording']['redis_port'] = '6379'
node.default['bigbluebutton']['recording']['steps'] = {
  archive: 'sanity',
  sanity: 'captions',
  captions: 'process:presentation',
  "process:presentation": 'publish:presentation',
}
node.default['bigbluebutton']['recording']['log_dir'] = '/var/log/bigbluebutton'
node.default['bigbluebutton']['recording']['events_dir'] = '/var/bigbluebutton/events'
node.default['bigbluebutton']['recording']['recording_dir'] = '/var/bigbluebutton/recording'
node.default['bigbluebutton']['recording']['published_dir'] = '/var/bigbluebutton/published'
node.default['bigbluebutton']['recording']['captions_dir'] = '/var/bigbluebutton/captions'
node.default['bigbluebutton']['recording']['playback_host'] = hostname
node.default['bigbluebutton']['recording']['playback_protocol'] = 'https'
