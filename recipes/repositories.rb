
node['bigbluebutton']['repo'].each do |name, repodata|
  apt_repository name do
    uri repodata['uri']
    key repodata['key']
    components repodata['components']
    distribution repodata['distribution'] if repodata['distribution']
    arch repodata['arch'] if repodata['arch']
  end
end
