# This recipe installs a script which collects feedback information from bbb-html5.
# The feedback will be written into a postgresql database to make it accessible
# for analysis.
vault_name = node['bigbluebutton']['vault_name']
vault_item = node['bigbluebutton']['vault_item']
vault = chef_vault_item(vault_name, vault_item)

feedback_db = vault['feedback']['db']

file '/etc/bigbluebutton/feedback.connstring' do
  content feedback_db
  mode '0600'
  sensitive true
end

package 'python3-psycopg2'
cookbook_file '/usr/local/sbin/bbb-feedback.py' do
  source 'feedback/bbb-feedback.py'
  mode '0755'
end
cookbook_file '/usr/local/sbin/bbb-feedback.sh' do
  source 'feedback/bbb-feedback.sh'
  mode '0755'
end
systemd_unit 'bbb-feedback.service' do
  action [:create, :enable, :start]
  content({
    'Unit': {
      'Description' => 'Extracts user feedback from HTML5 client logs and dumps them into a PostgreSQL database',
      'After' => 'network-online.target',
      'Wants' => 'network-online.target',
    },
    'Service': {
      'ExecStart' => '/usr/local/sbin/bbb-feedback.sh',
      'Restart' => 'always',
    },
    'Install': {
      'WantedBy' => 'multi-user.target',
    },
  })
end
