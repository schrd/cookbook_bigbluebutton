user 'bigbluebutton' do
  system true
  shell '/bin/false'
end

user 'meteor' do
  system true
  home '/usr/share/meteor'
end

user 'etherpad' do
  system true
  home '/usr/share/etherpad-lite'
  shell '/bin/false'
end

directory '/etc/bigbluebutton' do
  owner 'bigbluebutton'
  # this needs to be readable to anyone, so meteor can enter this directory
  mode '0755'
end

template '/etc/bigbluebutton/bbb-apps-akka.conf' do
  owner 'bigbluebutton'
  mode '0600'
  helpers BigBlueButtonHelper
  variables 'apps_akka': node['bigbluebutton']['apps_akka']
  sensitive true
end

template '/etc/bigbluebutton/bbb-fsesl-akka.conf' do
  owner 'bigbluebutton'
  mode '0600'
  helpers BigBlueButtonHelper
  variables 'fsesl_akka': node['bigbluebutton']['fsesl_akka']
  sensitive true
end

template '/etc/bigbluebutton/bbb-web.properties' do
  owner 'bigbluebutton'
  mode '0600'
  helpers BigBlueButtonJavaHelper
  variables 'bbb_web': node['bigbluebutton']['bbb_web']
  sensitive true
end

template '/etc/bigbluebutton/bbb-html5.yml' do
  owner 'meteor'
  group 'bigbluebutton'
  mode '0640'
  source 'bbb-html5.yml.erb'
  variables 'bbb_html5': YAML.dump(node['bigbluebutton']['bbb_html5'].to_hash)
  sensitive true
end

template '/etc/bigbluebutton/bbb-html5-with-roles.conf' do
  source 'bbb-html5-with-roles.conf.erb'
  variables frontends: node['bigbluebutton']['bbb_html5_services']['frontends'],
    backends: node['bigbluebutton']['bbb_html5_services']['backends']
end

directory '/usr/share/etherpad-lite' do
  owner 'etherpad'
end

file '/usr/share/etherpad-lite/APIKEY.txt' do
  content node['bigbluebutton']['bbb_html5']['private']['etherpad']['apikey']
  mode '0400'
  owner 'etherpad'
  sensitive true
end

directory '/etc/bigbluebutton/bbb-webrtc-sfu' do
  owner 'bigbluebutton'
end

template '/etc/bigbluebutton/bbb-webrtc-sfu/production.yml' do
  owner 'bigbluebutton'
  mode '0600'
  source 'bbb-webrtc-sfu.yml.erb'
  variables 'bbb_webrtc_sfu': YAML.dump(node['bigbluebutton']['bbb_webrtc_sfu'].to_hash)
  sensitive true
end

# this may take a long time. It is pulling all the dependencies and will create
# the libreoffice docker container
package 'bigbluebutton' do
  timeout 30 * 60
end

package 'bbb-html5'

# for performance reasons there should be exactly one turn server only
template '/etc/bigbluebutton/turn-stun-servers.xml' do
  owner 'bigbluebutton'
  mode '0600'
  sensitive true
  variables turnserver: node['bigbluebutton']['turnserver'],
    turnsecret: node['bigbluebutton']['turnsecret'],
    stunserver: node['bigbluebutton']['stunserver']
  only_if { !!((node['bigbluebutton']['turnserver'] and node['bigbluebutton']['turnsecret']) or node['bigbluebutton']['stunserver']) }
end

# setup kurento and freeswitch for multiple kurentos
# to do this we have to
# * replace the kurento-media-server unit by a template unit
# * add dependency from bbb-webrtc-sfu to the kurento instances
# * generate a shared DTLS key. This is required because otherwise firefox won't
#   be able to connect audio and video at the same time because firefox does some
#   kind of DTLS certificate pinning

execute 'systemctl daemon-reload' do
  action :nothing
end

if node['platform_version'] < '22.04'
  # generate shared dtls key for kurento/freeswitch
  certkey = generate_dtls_key_cert(node['fqdn'])
  file '/etc/kurento/dtls-srtp.pem' do
    action :create_if_missing
    content certkey
    owner 'kurento'
    sensitive true
  end

  directory '/opt/freeswitch/etc/freeswitch/tls' do
    recursive true
    owner 'freeswitch'
  end

  file '/opt/freeswitch/etc/freeswitch/tls/dtls-srtp.pem' do
    content lazy { ::File.read('/etc/kurento/dtls-srtp.pem') }
    owner 'freeswitch'
    sensitive true
  end

  cookbook_file '/etc/systemd/system/kurento-media-server@.service' do
    source 'systemd/kurento-media-server@.service'
    notifies :run, 'execute[systemctl daemon-reload]', :immediately
  end

  cookbook_file '/etc/systemd/system/kurento-media-server.service' do
    source 'systemd/kurento-media-server.service-override.conf'
    notifies :run, 'execute[systemctl daemon-reload]', :immediately
  end

  directory '/etc/systemd/system/bbb-webrtc-sfu.service.d'
  cookbook_file '/etc/systemd/system/bbb-webrtc-sfu.service.d/chef.conf' do
    source 'systemd/bbb-webrtc-sfu.service-override.conf'
    notifies :run, 'execute[systemctl daemon-reload]', :immediately
  end

  # configure kurento to use external IP in SDP
  template '/etc/kurento/modules/kurento/WebRtcEndpoint.conf.ini' do
    source 'kurento/WebRtcEndpoint.conf.ini.erb'
    variables host_ip: node['bigbluebutton']['bbb_webrtc_sfu']['kurento'][0]['ip'],
      certificate_path: '/etc/kurento/dtls-srtp.pem'
  end

  sysctl 'kernel.pid_max' do
    comment 'Kurento nees more processes so it can handle more video streams'
    value '419304'
  end

  %w(8888 8889 8890).each do |i|
    template "/etc/kurento/kurento-#{i}.conf.json" do
      source 'kurento/kurento.conf.json.erb'
      variables port: i
    end
  end
end
# enable and start services
%w(bbb-web bbb-webrtc-sfu bbb-html5 bbb-apps-akka bbb-fsesl-akka etherpad freeswitch).each do |s|
  service s do
    action [:enable, :start]
  end
end

# start html5 frontends/backends

if node['bigbluebutton']['version'] < '3.0'
  node['bigbluebutton']['bbb_html5_services']['backends'].times do |i|
    service "bbb-html5-backend@#{i + 1}" do
      action [:enable, :start]
    end
  end
  node['bigbluebutton']['bbb_html5_services']['frontends'].times do |i|
    service "bbb-html5-frontend@#{i + 1}" do
      action [:enable, :start]
    end
  end
  # enable multiple kurentos for community packages
  %w(8888 8889 8890).each do |i|
    service "kurento-media-server@#{i}" do
      action [:enable, :start]
    end
  end
else
  service 'bbb-html5' do
    action [:enable, :start]
  end
end
