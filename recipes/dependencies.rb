package %w(docker-ce docker-ce-cli containerd.io)

service 'docker' do
  action [:enable, :start]
end

package 'ruby'
gem_package 'bundler' do
  version '2.1.4'
  gem_binary '/usr/bin/gem'
end

package %w(nodejs mongodb-org haveged build-essential)

if node['platform_version'] != '20.04'
  package 'yq'
else
  remote_file '/usr/bin/yq' do
    source 'https://github.com/mikefarah/yq/releases/download/3.4.1/yq_linux_amd64'
    mode '0755'
  end
end

user 'redis' do
  system true
  home '/var/lib/redis'
  shell '/usr/sbin/nologin'
end

directory '/etc/redis' do
  user 'redis'
end

cookbook_file '/etc/redis/redis.conf' do
  source 'etc/redis/redis.conf'
  owner 'redis'
  mode '0600'
end

package 'redis'

service 'redis-server' do
  action [:enable, :start]
end
