# This recipe installs a cron-job which collects freeswitch logs.
# The logs will be written into a postgresql database to make it accessible
# for analysis.

vault_name = node['bigbluebutton']['vault_name']
vault_item = node['bigbluebutton']['vault_item']
vault = chef_vault_item(vault_name, vault_item)

feedback_db = vault['feedback']['db']

file '/etc/bigbluebutton/feedback.connstring' do
  content feedback_db
  mode '0600'
  sensitive true
end

package %w(python3-psycopg2 python3-urllib3 python3-pyasn)

cookbook_file '/usr/local/sbin/freeswitch_cdr_analyze.py' do
  source 'freeswitch/cdr_analyze.py'
  mode '0755'
end

cookbook_file '/usr/local/sbin/freeswitch_cdr_anonymize.py' do
  source 'freeswitch/cdr_anonymize.py'
  mode '0755'
end

cookbook_file '/opt/freeswitch/etc/freeswitch/autoload_configs/xml_cdr.conf.xml' do
  source 'freeswitch/xml_cdr.conf.xml'
  mode '0644'
end

# prepare and fetch route database if it does not exist
directory '/var/local/rib'

execute 'pyasn_util_download.py --latest' do
  cwd '/var/local/rib'
  only_if { ::Dir['/var/local/rib/rib*.bz2'].empty? }
end

execute 'pyasn_util_convert.py --single rib.*.bz2 rib.out' do
  cwd '/var/local/rib'
  not_if { ::File.exist?('/var/local/rib/rib.out') }
end

cron_d 'freeswitch_cdr_analyze' do
  mailto tuc_fu_admins(node['fqdn']).join(',')
  minute 21
  command 'python3 /usr/local/sbin/freeswitch_cdr_analyze.py $(find /opt/freeswitch/log/xml_cdr/ -mtime -1 -type f)'
  environment PATH: '/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin'
end

# cleanup cdr log files after 1 week
cron_d 'freeswitch_cdr_cleanup' do
  mailto tuc_fu_admins(node['fqdn']).join(',')
  hour 1
  minute 23
  command 'find /opt/freeswitch/log/xml_cdr_processed -mtime +7 -type f -print0 | xargs -0 /bin/rm -f'
end

# anonymize entries in cdr_entrys database table
# this will remove user name and IPs from the records.
cron_d 'freeswitch_cdr_anonymize' do
  mailto tuc_fu_admins(node['fqdn']).join(',')
  minute 20
  hour 1
  command "python3 /usr/local/sbin/freeswitch_cdr_anonymize.py #{node['bigbluebutton']['freeswitch']['host_ip']}"
end
