# clean attributes so no secrets are saved back to the chef server

ruby_block 'clean sensitive attributes' do
  block do
    node.rm(:bigbluebutton, :apps_akka, :services, :sharedSecret)
    node.rm(:bigbluebutton, :fsesl_akka, :freeswitch, :esl, :password)
    node.rm(:bigbluebutton, :freeswitch, :default_password)
    node.rm(:bigbluebutton, :freeswitch, :esl_password)
    node.rm(:bigbluebutton, :bbb_web, :securitySalt)
    node.rm(:bigbluebutton, :bbb_webrtc_sfu, :freeswitch, :esl_password)
    node.rm(:bigbluebutton, :bbb_html5, :private, :etherpad)
    node.rm(:bigbluebutton, :recording, :notes_apikey)
    node.rm(:bigbluebutton, :turnsecret)
  end
end
