default['bigbluebutton']['repo']['docker-ce'] = {
  uri: 'https://download.docker.com/linux/ubuntu',
  key: 'https://download.docker.com/linux/ubuntu/gpg',
  components: ['stable'],
  arch: 'amd64',
}
if node['platform_version'] == '18.04' # BBB 2.3, 2.4
  # only on 2.3, 2.4
  # for Ubuntu 20.04 BBB is shipping their own build
  default['bigbluebutton']['repo']['rmescandon-ubuntu-yq-bionic'] = {
    uri: 'ppa:rmescandon/yq',
    key: 'CC86BB64',
    components: ['main'],
  }
  default['bigbluebutton']['repo']['libreoffice-ubuntu-ppa-bionic'] = {
    uri: 'ppa:libreoffice/ppa',
    key: '1378B444',
    components: ['main'],
  }
  default['bigbluebutton']['repo']['bigbluebutton-ubuntu-support-bionic'] = {
    uri: 'ppa:bigbluebutton/support',
    key: 'E95B94BC',
    components: ['main'],
  }
  default['bigbluebutton']['repo']['kurento'] = {
    uri: 'http://ubuntu.openvidu.io/6.16.0',
    key: '5AFA7A83',
    components: ['kms6'],
    arch: 'amd64',
  }
  default['bigbluebutton']['repo']['nodesource'] = {
    uri: 'https://deb.nodesource.com/node_12.x',
    key: 'https://deb.nodesource.com/gpgkey/nodesource.gpg.key',
    components: ['main'],
  }
  default['bigbluebutton']['repo']['mongodb-org-4.2'] = {
    uri: 'https://repo.mongodb.org/apt/ubuntu',
    key: 'https://www.mongodb.org/static/pgp/server-4.2.asc',
    components: ['multiverse'],
    distribution: 'bionic/mongodb-org/4.2',
  }
  default['bigbluebutton']['repo']['bigbluebutton'] = {
    uri: 'https://ubuntu.bigbluebutton.org/bionic-240',
    key: 'https://ubuntu.bigbluebutton.org/repo/bigbluebutton.asc',
    components: ['main'],
    distribution: 'bigbluebutton-bionic',
  }

elsif node['platform_version'] == '20.04' # BBB 2.5
  default['bigbluebutton']['repo']['libreoffice-ubuntu-ppa-focal'] = {
    uri: 'ppa:libreoffice/ppa',
    key: '1378B444',
    components: ['main'],
  }
  default['bigbluebutton']['repo']['bigbluebutton-ubuntu-support-focal'] = {
    uri: 'ppa:bigbluebutton/support',
    key: 'E95B94BC',
    components: ['main'],
  }
  default['bigbluebutton']['repo']['nodesource'] = {
    uri: 'https://deb.nodesource.com/node_16.x',
    key: 'https://deb.nodesource.com/gpgkey/nodesource.gpg.key',
    components: ['main'],
  }
  default['bigbluebutton']['repo']['mongodb-org-4.4'] = {
    uri: 'https://repo.mongodb.org/apt/ubuntu',
    key: 'https://www.mongodb.org/static/pgp/server-4.4.asc',
    components: ['multiverse'],
    distribution: 'focal/mongodb-org/4.4',
  }
  default['bigbluebutton']['repo']['bigbluebutton'] = {
    uri: 'https://ubuntu.bigbluebutton.org/focal-25-dev',
    key: 'https://ubuntu.bigbluebutton.org/repo/bigbluebutton.asc',
    components: ['main'],
    distribution: 'bigbluebutton-focal',
  }
elsif node['platform_version'] == '22.04'
  default['bigbluebutton']['repo']['rmescandon-ubuntu-yq-jammy'] = {
    uri: 'ppa:rmescandon/yq',
    key: 'CC86BB64',
    components: ['main'],
  }
  default['bigbluebutton']['repo']['libreoffice-ubuntu-ppa-focal'] = {
    uri: 'ppa:libreoffice/ppa',
    key: '1378B444',
    components: ['main'],
  }
  default['bigbluebutton']['repo']['bigbluebutton-ubuntu-support-focal'] = {
    uri: 'ppa:bigbluebutton/support',
    key: 'E95B94BC',
    components: ['main'],
  }
  default['bigbluebutton']['repo']['nodesource'] = {
    uri: 'https://deb.nodesource.com/node_18.x',
    key: 'https://deb.nodesource.com/gpgkey/nodesource.gpg.key',
    components: ['main'],
  }
  default['bigbluebutton']['repo']['mongodb-org-6.0'] = {
    uri: 'https://repo.mongodb.org/apt/ubuntu',
    distribution: 'jammy/mongodb-org/6.0',
    components: ['multiverse'],
    key: 'https://pgp.mongodb.com/server-6.0.asc',
    # deb [ arch=amd64,arm64 signed-by=/usr/share/keyrings/mongodb-server-6.0.gpg ] https://repo.mongodb.org/apt/ubuntu jammy/mongodb-org/6.0 multiverse"
  }
  default['bigbluebutton']['repo']['bigbluebutton'] = {
    uri: 'https://ubuntu.bigbluebutton.org/jammy-30-dev',
    key: 'https://ubuntu.bigbluebutton.org/repo/bigbluebutton.asc',
    components: ['main'],
    distribution: 'bigbluebutton-jammy',
  }
end
