if node['platform_version'] == '18.04'
  default['bigbluebutton']['version'] = '2.4'
elsif node['platform_version'] == '20.04'
  default['bigbluebutton']['version'] = '2.5'
elsif node['platform_version'] == '22.04'
  default['bigbluebutton']['version'] = '3.0'
end
