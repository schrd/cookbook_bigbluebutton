# -*- coding: utf-8 -*-
from __future__ import unicode_literals, print_function

from datetime import datetime
from lxml import etree
import os
import os.path
import sys
import pyasn
import psycopg2
import urllib.parse

asndb = pyasn.pyasn('/var/local/rib/rib.out')

with open("/etc/bigbluebutton/feedback.connstring", "r") as f:
    connstring = f.readline()
conn = psycopg2.connect(connstring)
cur = conn.cursor()
cur.execute("""
    CREATE TABLE IF NOT EXISTS cdr_entries (
        call_uuid uuid primary key,
        remote_as_number integer,
        remote_ip inet,
        remote_port integer,
        local_ip inet,
        local_port integer,
        sip_from_user varchar,
        hangup_cause varchar,
        call_quality float,
        conference_number integer,
        start timestamp,
        ende timestamp
    )
""")
cur.execute("""
    CREATE TABLE if not exists cdr_errors (
        call_uuid uuid references cdr_entries(call_uuid) on delete cascade,
        start timestamp,
        ende timestamp,
        flaws integer,
        consecutive_flaws integer
    )
           """)

def get_values_from_node(node, name_map):
    data = {}
    for name in name_map:
        func = name_map[name]
        try:
            text = node.xpath(name)[0].text
        except IndexError:
            print("%s fehlt" % name)
            raise
        value = func(text)
        data[name] = value
    return data


def parse_file(filename):
    #print(filename)
    tree = etree.parse(filename)
    try:
        call_quality=tree.xpath('/cdr/call-stats/audio/inbound/quality_percentage')[0].text
    except IndexError:
        call_quality = None
    error_log = tree.xpath('/cdr/call-stats/audio/error-log/error-period')
    variables = tree.xpath('/cdr/variables')[0]
    if len(variables.xpath('vbridge')) == 0:
        return False

    variable_map = {
        'remote_audio_ip': str,
        'remote_audio_port': int,
        'local_media_ip': str,
        'local_media_port': int,
        'start_uepoch': lambda x: datetime.fromtimestamp(int(x) / 1000000),
        'end_uepoch': lambda x: datetime.fromtimestamp(int(x) / 1000000),
        'uuid': str,
        'sip_from_user': str,
        'vbridge': str,
        'hangup_cause': str,
    }

    try:
        data = get_values_from_node(variables, variable_map)
    except IndexError:
        # irgendein Feld fehlt
        print(filename)
        return False
    #remote_audio_ip = variables.xpath('remote_audio_ip')[0].text
    #remote_audio_port = variables.xpath('remote_audio_port')[0].text
    #sip_from_user = variables.xpath('sip_from_user')[0].text
    start = datetime.fromtimestamp(int(variables.xpath('start_uepoch')[0].text) / 1000000)
    ende = datetime.fromtimestamp(int(variables.xpath('end_uepoch')[0].text) / 1000000)
    remote_ip = urllib.parse.unquote(data['remote_audio_ip'])
    local_ip = urllib.parse.unquote(data['local_media_ip'])

    cur.execute("""INSERT INTO cdr_entries (
            call_uuid,
            remote_as_number, remote_ip, remote_port,
            local_ip, local_port,
            sip_from_user, hangup_cause, call_quality, conference_number,
            start, ende) VALUES (
        %s, 
        %s, %s, %s, 
        %s, %s, 
        %s, %s, %s, %s, 
        %s, %s
    )
    """, (
        data['uuid'],
        asndb.lookup(remote_ip)[0], remote_ip, data['remote_audio_port'],
        local_ip, data['local_media_port'],
        data['sip_from_user'], data['hangup_cause'], call_quality, data['vbridge'],
        data['start_uepoch'], data['end_uepoch'],
        )
    )

    error_map = {
        'start': lambda x: datetime.fromtimestamp(int(x) / 1000000),
        'stop': lambda x: datetime.fromtimestamp(int(x) / 1000000),
        'flaws': int,
        'consecutive-flaws': int,
    }
    for error in error_log:
        errors = get_values_from_node(error, error_map)
        start = int(error.xpath("start")[0].text)
        duration = int(error.xpath("duration-msec")[0].text)
        ts = datetime.fromtimestamp(start / 1000000)
        cur.execute("""
            INSERT INTO cdr_errors (
                call_uuid,
                start, ende,
                flaws, consecutive_flaws
            ) values (%s, %s, %s, %s, %s)
        """,
        (data['uuid'],
        errors['start'], errors['stop'],
        errors['flaws'], errors['consecutive-flaws']
        )
                )
    conn.commit()
    return True

error_dir = '/opt/freeswitch/log/xml_cdr_error'
processed_dir = '/opt/freeswitch/log/xml_cdr_processed'
if not os.path.exists(error_dir):
    os.mkdir(error_dir)
if not os.path.exists(processed_dir):
    os.mkdir(processed_dir)
for f in sys.argv[1:]:
    filename = os.path.basename(f)
    try:
        success = parse_file(f)
        if not success:
            os.rename(f, os.path.join(error_dir, filename))
        else:
            os.rename(f, os.path.join(processed_dir, filename))
    except etree.XMLSyntaxError:
        pass
    except psycopg2.errors.UniqueViolation:
        conn.rollback()
        os.rename(f, os.path.join(processed_dir, filename))
