# -*- coding: utf-8 -*-
from __future__ import unicode_literals, print_function

from datetime import datetime
import sys
import psycopg2

with open("/etc/bigbluebutton/feedback.connstring", "r") as f:
    connstring = f.readline()
conn = psycopg2.connect(connstring)
cur = conn.cursor()
cur.execute("""
    CREATE TABLE IF NOT EXISTS cdr_entries (
        call_uuid uuid primary key,
        remote_as_number integer,
        remote_ip inet,
        remote_port integer,
        local_ip inet,
        local_port integer,
        sip_from_user varchar,
        hangup_cause varchar,
        call_quality float,
        conference_number integer,
        start timestamp,
        ende timestamp
    )
""")
cur.execute("""
    CREATE TABLE if not exists cdr_errors (
        call_uuid uuid references cdr_entries(call_uuid) on delete cascade,
        start timestamp,
        ende timestamp,
        flaws integer,
        consecutive_flaws integer
    )
           """)

cur.execute("""
    UPDATE cdr_entries 
    SET remote_ip = null, remote_port = null, sip_from_user=null 
    WHERE local_ip=%s and
          start < 'today'::date - '1 weeks'::interval and 
          sip_from_user is not null and 
          remote_ip is not null and 
          remote_port is not null
           """,
        (sys.argv[1],)
           )
conn.commit()
