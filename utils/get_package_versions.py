#!/usr/bin/python3
# -*- coding: utf-8 -*-
from __future__ import unicode_literals, print_function
import json
import requests
import sys

def parse_packages(pins_dict, packages):
    for line in packages.splitlines():
        if line.startswith("Package: "):
            _tag, package = line.strip().split(" ")
        if line.startswith("Version: "):
            _tag, version = line.strip().split(" ")
            if version > pins_dict.get(packages, ""):
                pins_dict[package] = version

if len(sys.argv) == 2:
    version = sys.argv[1]
else:
    version = None


pins = {}
url = "http://ubuntu.openvidu.io/6.16.0/dists/bionic/kms6/binary-amd64/Packages"
parse_packages(pins, requests.get(url).text)
if version:
    if version.startswith("2.4"):
        url = "https://ubuntu.bigbluebutton.org/bionic-240-{}/dists/bigbluebutton-bionic/main/binary-amd64/Packages".format(version)
    else:
        url = "https://ubuntu.bigbluebutton.org/bionic-230-{}/dists/bigbluebutton-bionic/main/binary-amd64/Packages".format(version)
else:
    url = "https://ubuntu.bigbluebutton.org/bionic-230/dists/bigbluebutton-bionic/main/binary-amd64/Packages".format(version)

req = requests.get(url)

parse_packages(pins, req.text)

#lines = req.text.splitlines()
#for line in lines:
#    if line.startswith("Package: "):
#        _tag, package = line.strip().split(" ")
#    if line.startswith("Version: "):
#        _tag, version = line.strip().split(" ")
#        pins[package] = version

print(json.dumps(pins, indent=4))
